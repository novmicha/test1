<?

class init {

	protected $db;
	private static $instance = null;

	private function __construct() {	
		$this->db = mysqli_connect('db', 'dev', 'ferIsThincogmo', 'dev');		

		$res = mysqli_query($this->db, "SHOW TABLES LIKE 'test'");
		if($res->num_rows==0) {
			self::create();
			self::fill();
		}
	}

	static public function getInstance() {
		if(is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function create() {
		$sql = "CREATE TABLE `test` (
  					`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  					`script_name` varchar(25) NOT NULL,
  					`start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  					`sort_index` decimal(3,0) NOT NULL,
  					`result` enum('normal','illegal','failed','success') NOT NULL
				)";
		if(mysqli_query($this->db, $sql)){  
		    echo "Table test created successfully<br />";  
		} else {  
		    echo "Table test is not created successfully<br />";  
		} 
	}

	private function fill( $qtyRows=20 ) {
		for($i=1;$i<=$qtyRows;$i++) {

			$q = "INSERT INTO `test` (`script_name`, `start_time`, `sort_index`, `result`) 
			           VALUES ('".substr(sha1(microtime()+rand(100)),0,25)."', CURRENT_TIMESTAMP, ".rand(0, 999).", ".rand(1,4).");";
			if(!mysqli_query($this->db, $q)) echo mysqli_error($this->db);
		}
	}

	public function get() {

		$q = "SELECT * FROM `test` WHERE result IN ('normal', 'success')";
		$r = mysqli_query($this->db, $q);

		if($r) {			
			while($row = $r->fetch_object()) $res[] = $row;
		}
		else echo mysqli_error($this->db);

		return($res);

	}


}

?>